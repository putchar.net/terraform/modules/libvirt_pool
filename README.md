# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Types of changes:
- Added for new features.
- Changed for changes in existing functionality.
- Deprecated for soon-to-be removed features.
- Removed for now removed features.
- Fixed for any bug fixes.
- Security in case of vulnerabilities.

## [1.0.0] - 2021-08-13
### Added
- 00-terraform.tf, it is now mandatory to add every providers used inside the "module"
```hcl
terraform {
  required_version = "1.0.4"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.10"
    }
  }
}
```

- libvirt_pool.tf, added libvirt_pool resource
- variables.tf, added variables
