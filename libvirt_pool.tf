resource "libvirt_pool" "storage_pool" {
  name = var.storage_name
  type = var.storage_type
  path = var.storage_path
}

output "storage_pool_name" {
  value = libvirt_pool.storage_pool.name
}
