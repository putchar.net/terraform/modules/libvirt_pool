variable "storage_name" {
  type = string
}

variable "storage_type" {
  type = string
}

variable "storage_path" {
  type = string
}
